#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];

map<string, string> users;
CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}
void CryptoDevice::initmap()
{
	users.insert(pair<string, string>("nir", "FC00961C2FA94EA0CE1649DF3C81749D"));//kaufmann
	users.insert(pair<string, string>("dark", "C48C29157E2B358CC144027F3E2D8CB4"));//memes
	users.insert(pair<string, string>("f", "7B774EFFE4A349C6DD82AD4F4F21D34C"));//u
}
bool CryptoDevice::login(string user, string pass)
{
	string encPass = "";
	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)pass.c_str(), pass.length());
	CryptoPP::HexEncoder encoder;
	encoder.Attach(new CryptoPP::StringSink(encPass));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	return (users.find(user)!=users.end()&&users[user] == encPass);
}

string CryptoDevice::encryptAES(string plainText)
{

	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{

	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();


	return decryptedText;
}